defmodule Dummy.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      %{
        id: :dummy,
        start: {:mochiweb_http, :start, [[name: Dummy, loop: {Dummy, :loop}, port: 5000, max: 1_000_000]]},
        restart: :permanent,
        shutdown: :infinity,
        type: :supervisor
      }
    ]

    opts = [strategy: :one_for_one, name: Dummy.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
