#![deny(warnings)]
extern crate hyper;
extern crate pretty_env_logger;
extern crate rand;

use hyper::{Body, Request, Response, Server};
use hyper::service::service_fn_ok;
use hyper::rt::{self, Future};

use rand::Rng;

use std::{thread, time};

fn main() {
    pretty_env_logger::init();
    let addr = ([127, 0, 0, 1], 5000).into();

    let server = Server::bind(&addr)
        .serve(|| {
            service_fn_ok(move |_: Request<Body>| {
                let mut rng = rand::thread_rng();
                let sleep_duration = time::Duration::from_millis(100 + (5 - rng.gen_range(0, 10)));

                thread::sleep(sleep_duration);

                Response::new(Body::from("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mollis dictum ligula, ut sagittis nisl malesuada nec. Fusce hendrerit leo augue, nec pretium dolor porta sodales. Sed consequat sed purus eu aliquet. Etiam laoreet nibh vel ex sodales, non egestas lorem tempor. Pellentesque placerat facilisis felis, nec bibendum metus finibus quis. Donec lobortis, sapien at tristique placerat, nibh libero volutpat eros, eget mollis nibh elit et enim. Vestibulum consequat ut lorem sed eleifend. Ut eu dolor ut lectus faucibus rhoncus. Nam vestibulum vitae massa vel congue. Nam ac odio lacus. Nam condimentum ante eget mollis vestibulum. Cras nisi sapien, tempor nec diam at, vulputate cursus odio. Maecenas vitae tellus efficitur arcu mollis ultrices id vitae ex. Suspendisse potenti. Duis nec vestibulum dui. Donec ultricies sit amet lorem eu feugiat. Ut pretium vitae lectus at tempor. Curabitur condimentum arcu varius nulla ultricies, id feugiat odio dictum. Vivamus sollicitudin consectetur nullam."))
            })
        })
        .map_err(|e| eprintln!("server error: {}", e));

    println!("Listening on http://{}", addr);

    rt::run(server);
}
