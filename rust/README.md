Install Rust and Cargo:

    $ curl -sSf https://static.rust-lang.org/rustup.sh | sh

Build and run dummy:

    $ cargo run --release
