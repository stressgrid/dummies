package com.stressgrid.netty.dummy;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.ThreadLocalRandom;

import static io.netty.buffer.Unpooled.copiedBuffer;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.util.HashedWheelTimer;
import io.netty.util.Timer;
import io.netty.util.TimerTask;
import io.netty.util.Timeout;

public class App
{
    private static final byte[] PAYLOAD = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mollis dictum ligula, ut sagittis nisl malesuada nec. Fusce hendrerit leo augue, nec pretium dolor porta sodales. Sed consequat sed purus eu aliquet. Etiam laoreet nibh vel ex sodales, non egestas lorem tempor. Pellentesque placerat facilisis felis, nec bibendum metus finibus quis. Donec lobortis, sapien at tristique placerat, nibh libero volutpat eros, eget mollis nibh elit et enim. Vestibulum consequat ut lorem sed eleifend. Ut eu dolor ut lectus faucibus rhoncus. Nam vestibulum vitae massa vel congue. Nam ac odio lacus. Nam condimentum ante eget mollis vestibulum. Cras nisi sapien, tempor nec diam at, vulputate cursus odio. Maecenas vitae tellus efficitur arcu mollis ultrices id vitae ex. Suspendisse potenti. Duis nec vestibulum dui. Donec ultricies sit amet lorem eu feugiat. Ut pretium vitae lectus at tempor. Curabitur condimentum arcu varius nulla ultricies, id feugiat odio dictum. Vivamus sollicitudin consectetur nullam.".getBytes();

    private ChannelFuture channel;
    private final EventLoopGroup masterGroup;
    private final EventLoopGroup workerGroup;
    private final Timer timer;
    
    public App()
    {
        masterGroup = new NioEventLoopGroup();
        workerGroup = new NioEventLoopGroup();        
        timer = new HashedWheelTimer(1, TimeUnit.MILLISECONDS);
    }

    public void run()
    {
        Runtime.getRuntime().addShutdownHook(new Thread(){
            @Override
            public void run() { shutdown(); }
        });
        
        try
        {
            final ServerBootstrap bootstrap =
                new ServerBootstrap()
                    .group(masterGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>(){
                        @Override
                        public void initChannel(final SocketChannel ch) throws Exception
                        {
                            ch.pipeline().addLast(new HttpServerCodec());
                            ch.pipeline().addLast(new HttpObjectAggregator(512 * 1024));
                            ch.pipeline().addLast(new SimpleChannelInboundHandler<FullHttpRequest>()
                            {
                                @Override
                                protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest request)
                                        throws Exception
                                {
                                    timer.newTimeout(new TimerTask(){
                                        @Override
                                        public void run(Timeout timeout) {
                                            FullHttpResponse response = new DefaultFullHttpResponse(
                                                HttpVersion.HTTP_1_1,
                                                HttpResponseStatus.OK,
                                                copiedBuffer(PAYLOAD)
                                            );

                                            if (HttpHeaders.isKeepAlive(request))
                                            {
                                                response.headers().set(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE);
                                            }
                                            response.headers().set(HttpHeaders.Names.CONTENT_TYPE, "text/plain");
                                            response.headers().set(HttpHeaders.Names.CONTENT_LENGTH, PAYLOAD.length);
                                            
                                            ctx.writeAndFlush(response);
                                        }
                                    }, 100 + (5 - ThreadLocalRandom.current().nextInt(0, 10)), TimeUnit.MILLISECONDS);
                                }

                                @Override
                                public void exceptionCaught(final ChannelHandlerContext ctx, final Throwable cause) {
                                    ctx.close();
                                }
                            });
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);
            channel = bootstrap.bind(5000).sync();

            System.out.println("Listening on port 5000");
        }
        catch (final InterruptedException e) { }
    }
    
    public void shutdown()
    {
        System.out.println("Shutting down");

        workerGroup.shutdownGracefully();
        masterGroup.shutdownGracefully();

        try
        {
            channel.channel().closeFuture().sync();
        }
        catch (InterruptedException e) { }
    }

    public static void main(String[] args)
    {
        new App().run();
    }
}