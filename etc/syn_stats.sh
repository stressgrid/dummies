#!/bin/bash
echo "ts,q" > syn_stats.csv
while :
do
  TS="$(date +%s)"
  Q="$(ss -n state syn-recv sport = :5000 | wc -l)"
  echo "${TS},${Q}" >> syn_stats.csv
  sleep 1
done
