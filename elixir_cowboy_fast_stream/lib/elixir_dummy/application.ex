defmodule Dummy.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      %{
        id: :dummy,
        start:
          {:cowboy, :start_clear,
           [
             :dummy,
             %{num_acceptors: 10, max_connections: 999_999, socket_opts: [port: 5000]},
             %{
               max_keepalive: 1_000,
               max_received_frame_rate: {1_000_000, 1},
               stream_handlers: [Dummy.FastStream]
             }
           ]},
        restart: :permanent,
        shutdown: :infinity,
        type: :supervisor
      }
    ]

    opts = [strategy: :one_for_one, name: Dummy.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
