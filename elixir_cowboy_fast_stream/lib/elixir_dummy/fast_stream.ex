defmodule Dummy.FastStream do
  @moduledoc false

  @behaviour :cowboy_stream

  @body "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mollis dictum ligula, ut sagittis nisl malesuada nec. Fusce hendrerit leo augue, nec pretium dolor porta sodales. Sed consequat sed purus eu aliquet. Etiam laoreet nibh vel ex sodales, non egestas lorem tempor. Pellentesque placerat facilisis felis, nec bibendum metus finibus quis. Donec lobortis, sapien at tristique placerat, nibh libero volutpat eros, eget mollis nibh elit et enim. Vestibulum consequat ut lorem sed eleifend. Ut eu dolor ut lectus faucibus rhoncus. Nam vestibulum vitae massa vel congue. Nam ac odio lacus. Nam condimentum ante eget mollis vestibulum. Cras nisi sapien, tempor nec diam at, vulputate cursus odio. Maecenas vitae tellus efficitur arcu mollis ultrices id vitae ex. Suspendisse potenti. Duis nec vestibulum dui. Donec ultricies sit amet lorem eu feugiat. Ut pretium vitae lectus at tempor. Curabitur condimentum arcu varius nulla ultricies, id feugiat odio dictum. Vivamus sollicitudin consectetur nullam."

  def init(_stream_id, req, _opts) do
    Process.sleep(trunc(100 * (1.0 + 0.5 * (:rand.uniform() * 2.0 - 1.0))))

    headers = %{
      "content-type" => "text/plain",
      "content-length" => Integer.to_string(byte_size(@body))
    }

    {[{:response, 200, :cowboy_req.response_headers(headers, req), @body}, :stop], :undefined}
  end

  def data(stream_id, is_fin, data, next) do
    :cowboy_stream.data(stream_id, is_fin, data, next)
  end

  def info(stream_id, info, next) do
    :cowboy_stream.info(stream_id, info, next)
  end

  def terminate(stream_id, reason, next) do
    :cowboy_stream.terminate(stream_id, reason, next)
  end

  def early_error(stream_id, reason, partial_req, resp, opts) do
    :cowboy_stream.early_error(stream_id, reason, partial_req, resp, opts)
  end
end
