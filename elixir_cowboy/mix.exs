defmodule Dummy.MixProject do
  use Mix.Project

  def project do
    [
      app: :elixir_dummy,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {Dummy.Application, []}
    ]
  end

  defp deps do
    [
      {:cowboy, "~> 2.7.0"}
      # {:cowboy, git: "https://github.com/ninenines/cowboy.git", branch: "active_n"}
      # {:cowlib, git: "https://github.com/ninenines/cowlib.git", branch: "faster-http2", override: true}
    ]
  end
end
