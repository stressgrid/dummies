Build:

    $ mvn compile
    $ mvn package

Run:

    $ java -cp target/rapidoid_dummy-1.0-SNAPSHOT-jar-with-dependencies.jar com.stressgrid.rapidoid.dummy.App production=true on.port=5000 on.address=0.0.0.0